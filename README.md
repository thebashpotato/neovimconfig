<div align="center">
  <h1>Neovim config</h1>
  <img src="assets/neovim.png">
  <br>
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

The accumulation and result from 8 years of using vim

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

> use the python installation script (assumes a unix environment)

```bash
usage: installer [options]

Install neovim config on supported platforms and include optional parameters

optional arguments:
  -h, --help       show this help message and exit
  -i, --install
  -u, --uninstall
  --rustup         Install the rustup toolchain, and install sefr command line search engine program for neovim
  --nvm            Install node version manager

I hope you enjoy my config, please submit a pull request or open an issue for improvements
```

## Usage

TODO

## Maintainers

[@thebashpotato](https://github.com/thebashpotato)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

AGPL © 2022 Matt Williams
